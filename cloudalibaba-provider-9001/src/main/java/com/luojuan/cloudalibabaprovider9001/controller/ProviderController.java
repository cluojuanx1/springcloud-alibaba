package com.luojuan.cloudalibabaprovider9001.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
public class ProviderController {

    @GetMapping("/provider/nacos/hello/{id}")
    public String hello(@PathVariable("id")String id){
        System.out.println("aaaaa");
        return "hello 9001"+id;
    }

}
