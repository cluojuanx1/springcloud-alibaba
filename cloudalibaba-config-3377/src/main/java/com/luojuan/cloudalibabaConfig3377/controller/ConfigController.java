package com.luojuan.cloudalibabaConfig3377.controller;

import com.luojuan.cloudalibabaConfig3377.feign.FeignTestClient;
import com.luojuan.cloudalibabaConfig3377.feign.FeignUtils;
import com.luojuan.cloudalibabaConfig3377.feign.IFeginClientTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.FeignClientBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigController {

    @Autowired
    FeignTestClient feignTestClient;

    @Value("${config.info}")
    private String configInfo;

    @Value("${feign.url}")
    private String url;

    @GetMapping("/getinfo")
    public String getinfo(){
        return configInfo;
    }

    @GetMapping("/feignTest")
    public void feignTest(){
        System.out.println(feignTestClient.hello("111"));
    }

    @GetMapping("/feignTest2")
    public void feignTest2(){
        IFeginClientTemplate feginClientTemplate =  FeignUtils.buildClent("nacos-provider","http://localhost:9001/provider/nacos/hello/1", IFeginClientTemplate.class);
        System.out.println(feginClientTemplate.hello("111"));
    }
}
