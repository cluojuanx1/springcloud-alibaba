package com.luojuan.cloudalibabaConfig3377.feign;

import org.springframework.web.bind.annotation.*;

public interface IFeginClientTemplate {
    @GetMapping
    public String hello(@RequestParam(name = "id") String id);
}
