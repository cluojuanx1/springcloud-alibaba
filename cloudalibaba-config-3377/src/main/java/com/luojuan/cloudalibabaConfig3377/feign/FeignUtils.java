package com.luojuan.cloudalibabaConfig3377.feign;

import com.luojuan.cloudalibabaConfig3377.common.applicationContext.ApplicationContextBuilder;
import org.springframework.beans.BeansException;
import org.springframework.cloud.openfeign.FeignClientBuilder;
import org.springframework.cloud.openfeign.FeignContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class FeignUtils implements ApplicationContextAware {

    private static ApplicationContext applicationContext = null;

    private static Map<String,Object> FEGIN_CACHE = new ConcurrentHashMap<>();

    public static <T> T buildClent(String serverName,String url,Class<T> targetClass){
        T t = (T) FEGIN_CACHE.get(serverName);
        if(Objects.isNull(t)){
            FeignClientBuilder.Builder<T> builder = new FeignClientBuilder(applicationContext)
                    .forType(targetClass,serverName)
                    .url(url);
            t = builder.build();
            FEGIN_CACHE.put(serverName,t);
        }
        return t;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if(FeignUtils.applicationContext == null){
            FeignUtils.applicationContext = applicationContext;
        }
    }
}
