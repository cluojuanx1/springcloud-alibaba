package com.luojuan.cloudalibabaConfig3377.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("nacos-provider")
public interface FeignTestClient {
    @GetMapping("/provider/nacos/hello/{id}")
    public String hello(@PathVariable("id")String id);
}
