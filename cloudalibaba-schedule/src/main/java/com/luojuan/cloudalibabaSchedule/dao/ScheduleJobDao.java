package com.luojuan.cloudalibabaSchedule.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luojuan.cloudalibabaSchedule.model.ScheduleJobEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScheduleJobDao extends BaseMapper<ScheduleJobEntity> {
}
