package com.luojuan.cloudalibabaSchedule.utils;

import com.luojuan.cloudalibabaSchedule.model.ScheduleJobEntity;
import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;

import java.util.HashMap;
import java.util.Map;

public class ScheduleJob extends QuartzJobBean {
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        ScheduleJobEntity scheduleJob = (ScheduleJobEntity) jobExecutionContext.getMergedJobDataMap().get("JOB_PARAM_KEY");
        String requestUrl = scheduleJob.getRequestUrl();
        Map requestParam = FastJsonUtils.toMap(scheduleJob.getParams());
        if(requestParam == null){
            requestParam = new HashMap();
        }
        HttpEntity requestEntity = createRequestEntity(requestParam);
        try{
            ListenableFuture restRequest;
            switch (HttpMethod.resolve(scheduleJob.getRequestMethod())){
                case GET:
                    if(StringUtils.isNotEmpty(scheduleJob.getParams())){
                        StringBuffer param = new StringBuffer();
                        requestParam.forEach((k,v)->{
                            param.append("&");
                            param.append(k);
                            param.append("=");
                            param.append(v);
                        });
                        if(param.length()>0 && StringUtils.indexOf(scheduleJob.getRequestUrl(),"?")==-1){
                            param.setCharAt(0,'?');
                        }
                        requestUrl = requestUrl + param.toString();
                    }
                    restRequest = this.getAsyncRestTemplate().exchange(requestUrl,HttpMethod.GET,requestEntity,String.class,new Object[0]);
                    break;
                case POST:
                    restRequest = this.getAsyncRestTemplate().exchange(requestUrl,HttpMethod.POST,requestEntity,String.class,new Object[0]);
                    break;
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }


    }

    AsyncRestTemplate getAsyncRestTemplate() {
        AsyncRestTemplate asyncRestTemplate = (AsyncRestTemplate)SpringContextUtils.getBean(AsyncRestTemplate.class);
        return asyncRestTemplate;
    }

    private HttpEntity<Map> createRequestEntity(Map requestParam){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity requestEntity = new HttpEntity(requestParam, headers);
        return requestEntity;
    }
}
