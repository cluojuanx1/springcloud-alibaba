package com.luojuan.cloudalibabaSchedule.utils;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;

import java.io.IOException;
import java.lang.reflect.Type;

public class DataFormatSerializer extends SimpleDateFormatSerializer {
    public DataFormatSerializer(String pattern) {
        super(pattern);
    }

    public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
        if (object == null) {
            serializer.out.writeString("");
        } else {
            super.write(serializer, object, fieldName, fieldType, features);
        }

    }
}
