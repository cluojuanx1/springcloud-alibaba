package com.luojuan.cloudalibabaSchedule.utils;

import com.luojuan.cloudalibabaSchedule.enums.ScheduleStatus;
import com.luojuan.cloudalibabaSchedule.model.ScheduleJobEntity;
import org.quartz.*;

public class ScheduleUtils {

    public static void run(Scheduler scheduler, ScheduleJobEntity scheduleJob) {
        try {
            JobDataMap dataMap = new JobDataMap();
            dataMap.put("JOB_PARAM_KEY", scheduleJob);
            scheduler.triggerJob(getJobKey(scheduleJob.getJobId()), dataMap);
        } catch (SchedulerException var3) {
            throw new RuntimeException("立即执行定时任务失败", var3);
        }
    }

    public static JobKey getJobKey(Long jobId) {
        return JobKey.jobKey("TASK_" + jobId);
    }

    public static TriggerKey getTriggerKey(Long jobId) {
        return TriggerKey.triggerKey("TASK_" + jobId);
    }

    public static void createScheduleJob(Scheduler scheduler, ScheduleJobEntity scheduleJob) {
        try{
            JobDetail jobDetail = JobBuilder.newJob(ScheduleJob.class).withIdentity(getJobKey(scheduleJob.getJobId())).build();
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(scheduleJob.getCronExpression()).withMisfireHandlingInstructionDoNothing();
            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(getTriggerKey(scheduleJob.getJobId())).withSchedule(scheduleBuilder).build();
            jobDetail.getJobDataMap().put("JOB_PARAM_KEY",scheduleJob);
            scheduler.scheduleJob(jobDetail,trigger);
            if (scheduleJob.getStatus() == ScheduleStatus.PAUSE.getValue()) {
                pauseJob(scheduler, scheduleJob.getJobId());
            }
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public static void pauseJob(Scheduler scheduler, Long jobId) {
        try {
            scheduler.pauseJob(getJobKey(jobId));
        } catch (SchedulerException var3) {
            throw new RuntimeException(var3);
        }
    }

    public static CronTrigger getCronTrigger(Scheduler scheduler, Long jobId) {
        try {
            return (CronTrigger)scheduler.getTrigger(getTriggerKey(jobId));
        } catch (SchedulerException var3) {
            throw new RuntimeException(var3);
        }
    }

    public static void updateScheduleJob(Scheduler scheduler, ScheduleJobEntity scheduleJob) {
        try {
            TriggerKey triggerKey = getTriggerKey(scheduleJob.getJobId());
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(scheduleJob.getCronExpression()).withMisfireHandlingInstructionDoNothing();
            CronTrigger trigger = getCronTrigger(scheduler, scheduleJob.getJobId());
            trigger = (CronTrigger)trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
            trigger.getJobDataMap().put("JOB_PARAM_KEY", scheduleJob);
            scheduler.rescheduleJob(triggerKey, trigger);
            if (scheduleJob.getStatus() == ScheduleStatus.PAUSE.getValue()) {
                pauseJob(scheduler, scheduleJob.getJobId());
            }

        } catch (Exception var5) {
            throw new RuntimeException(var5);
        }
    }
}
