package com.luojuan.cloudalibabaSchedule.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.luojuan.cloudalibabaSchedule.dao.ScheduleJobDao;
import com.luojuan.cloudalibabaSchedule.enums.ScheduleStatus;
import com.luojuan.cloudalibabaSchedule.model.ScheduleJobEntity;
import com.luojuan.cloudalibabaSchedule.service.ScheduleJobService;
import com.luojuan.cloudalibabaSchedule.utils.ScheduleUtils;
import org.quartz.CronTrigger;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class ScheduleJobServiceImpl extends ServiceImpl<ScheduleJobDao, ScheduleJobEntity> implements ScheduleJobService {
    @Autowired
    Scheduler scheduler;

    @PostConstruct
    public void init() {
        List<ScheduleJobEntity> scheduleJobList = ((ScheduleJobDao)this.baseMapper).selectList((Wrapper)null);
        Iterator var2 = scheduleJobList.iterator();

        while(var2.hasNext()) {
            ScheduleJobEntity scheduleJob = (ScheduleJobEntity)var2.next();
            CronTrigger cronTrigger = ScheduleUtils.getCronTrigger(this.scheduler, scheduleJob.getJobId());
            if (cronTrigger == null) {
                ScheduleUtils.createScheduleJob(this.scheduler, scheduleJob);
            } else {
                ScheduleUtils.updateScheduleJob(this.scheduler, scheduleJob);
            }
        }

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void run(Long[] jobIds) {
        int len = jobIds.length;

        for(int i=0; i<len; i++){
            Long jobId = jobIds[i];
            ScheduleUtils.run(scheduler,this.baseMapper.selectById(jobId));
        }
    }

    @Override
    public void saveSchedule(ScheduleJobEntity scheduleJob) {
        scheduleJob.setCreateTime(new Date());
        scheduleJob.setStatus(ScheduleStatus.NORMAL.getValue());
        this.baseMapper.insert(scheduleJob);
        ScheduleUtils.createScheduleJob(this.scheduler,scheduleJob);
    }
}
