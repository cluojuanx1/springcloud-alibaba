package com.luojuan.cloudalibabaSchedule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.luojuan.cloudalibabaSchedule.model.ScheduleJobEntity;

public interface ScheduleJobService extends IService<ScheduleJobEntity> {

    void run(Long[] jobIds);

    void saveSchedule(ScheduleJobEntity scheduleJob);
}
