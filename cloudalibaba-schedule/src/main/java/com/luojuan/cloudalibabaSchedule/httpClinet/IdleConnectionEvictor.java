package com.luojuan.cloudalibabaSchedule.httpClinet;

import org.apache.http.conn.HttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;

public class IdleConnectionEvictor extends Thread{
    @Autowired
    private HttpClientConnectionManager connMgr;
    private volatile boolean shutdown;

    public IdleConnectionEvictor() {
        super.start();
    }

    public void run() {
        while(true) {
            try {
                if (!this.shutdown) {
                    synchronized(this) {
                        this.wait(5000L);
                        this.connMgr.closeExpiredConnections();
                        continue;
                    }
                }
            } catch (InterruptedException var4) {
            }

            return;
        }
    }

    public void shutdown() {
        this.shutdown = true;
        synchronized(this) {
            this.notifyAll();
        }
    }
}
