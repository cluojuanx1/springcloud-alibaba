package com.luojuan.cloudalibabaSchedule.httpClinet;

public class HttpResult {
    private int code;
    private String result;

    public HttpResult() {
    }

    public HttpResult(int code, String result) {
        this.code = code;
        this.result = result;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
