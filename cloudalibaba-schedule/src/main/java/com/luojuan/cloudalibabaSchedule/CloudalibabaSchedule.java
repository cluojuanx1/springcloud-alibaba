package com.luojuan.cloudalibabaSchedule;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.luojuan.cloudalibabaSchedule.dao")
public class CloudalibabaSchedule {

    public static void main(String[] args) {
        SpringApplication.run(CloudalibabaSchedule.class, args);
    }

}
