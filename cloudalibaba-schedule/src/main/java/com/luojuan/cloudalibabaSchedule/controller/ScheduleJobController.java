package com.luojuan.cloudalibabaSchedule.controller;

import com.luojuan.cloudalibabaSchedule.model.ScheduleJobEntity;
import com.luojuan.cloudalibabaSchedule.service.ScheduleJobService;
import com.luojuan.cloudalibabaSchedule.utils.FastJsonUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ScheduleJobController {

    @Autowired
    ScheduleJobService scheduleJobService;

    @PostMapping({"/save"})
    public ScheduleJobEntity save(@RequestBody ScheduleJobEntity scheduleJob){
        if(StringUtils.isNotEmpty(scheduleJob.getParams())){
            Map map = null;

            try {
                map = (Map) FastJsonUtils.toBean(scheduleJob.getParams(), Map.class);
            }catch (Exception e){
                map = null;
            }

            if(map == null){
                throw new RuntimeException("参数不是正确的json格式");
            }
        }
        scheduleJobService.saveSchedule(scheduleJob);
        return scheduleJob;
    }

    @PostMapping("run")
    public void run(@RequestBody Long[] jobIds){
        this.scheduleJobService.run(jobIds);
    }
}
