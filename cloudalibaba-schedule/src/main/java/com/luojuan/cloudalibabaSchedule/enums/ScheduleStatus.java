package com.luojuan.cloudalibabaSchedule.enums;

public enum ScheduleStatus {
    NORMAL(1),
    PAUSE(2);

    private int value;
    private ScheduleStatus(int value){
        this.value = value;
    }
    public int getValue(){
        return value;
    }
}
