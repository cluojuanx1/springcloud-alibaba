package com.luojuan.cloudalibabaSchedule.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sun.istack.internal.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("schedule_job")
public class ScheduleJobEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final String JOB_PARAM_KEY = "JOB_PARAM_KEY";
    @TableId
    @JSONField(
            serializeUsing = ToStringSerializer.class
    )
    private Long jobId;
    //请求地址
    @NotNull
    private String requestUrl;
    //请求方法
    @NotNull
    private String requestMethod;
    //请求参数
    private String params;
    //cron表达式
    @NotNull
    private String cronExpression;
    //任务状态
    private Integer status;
    //创建时间
    private Date createTime;
}
