package com.luojuan.cloudalibabaprovider9002.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProviderController {

    @GetMapping("/provider/nacos/hello/{id}")
    public String hello(@PathVariable("id")String id){
        return "hello 9002"+id;
    }
}
