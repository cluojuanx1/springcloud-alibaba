package com.luojuan.cloudalibabaconsumer83.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
public class ConsumerController {

    @Resource
    private RestTemplate restTemplate;

    @Value("${service-url.nacos-user-service}")
    private String SERVER_URL;

    @GetMapping("/consumer/nacos/hello/{id}")
    public String hello(@PathVariable("id")String id){
        return restTemplate.getForObject(SERVER_URL+"/nacos/hello/"+id,String.class);
    }
}
